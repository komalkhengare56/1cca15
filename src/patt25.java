//93
/*
N       N
 N     N
  N   N
   N N
    N
 */
public class patt25 {
    public static void main(String[] args) {
        int line = 5;
        int col = 9;
        int space = 0;
        for (int i = 0; i < line; i++) {

            for (int k = 0; k < space; k++)
                System.out.print(" ");
            for (int j = 0; j < col; j++)
                if (j == 0 || j == col - 1)
                    System.out.print("N");
                else
                    System.out.print(" ");
            System.out.println();
            col -= 2;
            space++;
        }
    }
}
