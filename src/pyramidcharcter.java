public class pyramidcharcter {
    public static void main(String[]args){
        int row=5;
        int col=1;
        int space=4;
        int ch=1;

        for(int i=0;i<row;i++){
            for(int k=0;k<space;k++)
                System.out.print(" ");
            for(int j=0;j<col;j++){
                if(i==j)
                    System.out.print(ch);
                else
                    System.out.print("*");


            }
            System.out.println();
            ch++;

            col+=2;
            space--;
        }
    }

}
