/*
     5
   45
  345
 2345
12345
 */
public class trianglePatt3 {
    public static void main(String[] args) {
        int row = 5;
        int col = 1;
        int ch=5;
        int space = row - 1;

        for (int i = 0; i <row; i++) {

            for (int k = 0; k < space; k++) {
                System.out.print(" ");
            }
            int ch1=ch;
            for (int j = 0; j < col; j++) {

                System.out.print(ch1++);
            }
            ch--;
            System.out.println();
            space--;
            col++;


        }

    }
}


