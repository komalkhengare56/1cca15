public class integerpattern3 {
    public static void main(String[]args){
        int row=7;
        int col=1;
        for(int i=0;i<row;i++){
            int ch=3;
            for(int j=0;j<col;j++){
                System.out.print(ch--+" ");
            }
            System.out.println();
            if(i<=2){
                col++;
            }
            else{
                col--;
            }
        }
    }
}
