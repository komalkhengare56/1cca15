public class integerPattern2 {
    public static void main(String[]args){
        int row=7;
        int col=1;
        int ch=3;
        for(int i=0;i<row;i++){
            int ch1=ch;
            for(int j=0;j<col;j++){
                System.out.print(ch1++);
            }
            System.out.println();
            if(i<=2){
                ch--;
                col++;
            }
            else{
                ch++;
                col--;
            }
        }
    }
}
