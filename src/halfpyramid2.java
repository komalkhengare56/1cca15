public class halfpyramid2 {
    public static void main(String[]args){
        int line =7;
        int col=1;
        //int space=4;
        for(int i=0;i<line;i++){
            for(int j=0;j<col;j++) {
                if (j == col-1|| j == col-2)
                    System.out.print("*"+" ");
                else
                    System.out.print(" ");
            }
            System.out.println();
            if(i<=2) {
                col++;
            }
            else {
                col--;
            }
        }
    }
}
