public class hollowdiamond {
    public static void main(String[] args) {
        int row = 5;
        int col = 1;
        int space = 2;
        for (int i = 0; i < row; i++) {
            for (int k = 0; k < space; k++)
                System.out.print(" ");
            for(int j=0;j<col;j++)
            {
                if(j==0||j==col-1)
                 System.out.print("*"+" ");
                else{
                    System.out.print(" ");
                }


            }
            System.out.println();
            if(i<=1)
            {
                col+=2;
                space--;
            }
            else {
                col-=2;
                space++;
            }
        }
    }
}