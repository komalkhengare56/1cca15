//32
/*
FEDCBA
FEDCB
FEDC
FED
FE
F

 */
public class charpatt5 {
    public static void main(String[]args){
        int row=6;
        int col=6;
        for(int i=0;i<row;i++){
            char ch='F';
            for(int j=0;j<col;j++){
                System.out.print(ch--);
            }
            System.out.println();
            col--;
        }
    }
}
